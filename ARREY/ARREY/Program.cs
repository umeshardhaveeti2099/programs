﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARREY
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 10, 20, 30, 40, 50 };
            SortArray(ref arr);

            Console.WriteLine("Initial sorted array:");
            PrintArray(arr);

            // Insert an element
            InsertElement(ref arr, 15);
            Console.WriteLine("\nArray after inserting 15:");
            PrintArray(arr);

            // Delete an element
            DeleteElement(ref arr, 30);
            Console.WriteLine("\nArray after deleting 30:");
            PrintArray(arr);

            // Update an element
            UpdateElement(ref arr, 40, 45);
            Console.WriteLine("\nArray after updating 40 to 45:");
            PrintArray(arr);

            // Reverse the array
            ReverseArray(ref arr);
            Console.WriteLine("\nArray after reversing:");
            PrintArray(arr);

            // Linear search
            int linearSearchResult = LinearSearch(arr, 10);
            Console.WriteLine($"\nLinear search for 25: {linearSearchResult}");
           

            // Binary search
            int binarySearchResult = BinarySearch(arr, 45);
            Console.WriteLine($"Binary search for 45: {binarySearchResult}");
            Console.ReadLine();
        }

        static void PrintArray(int[] arr)
        {
            foreach (int num in arr)
            {
                Console.Write(num + " ");
            }
            Console.WriteLine();
            
        }

        static void InsertElement(ref int[] arr, int element)
        {
            int n = arr.Length;
            int[] newArr = new int[n + 1];

            // Find the index where the element should be inserted
            int i = 0;
            while (i < n && arr[i] < element)
            {
                newArr[i] = arr[i];
                i++;
            }

            // Insert the element
            newArr[i] = element;

            // Copy the remaining elements
            for (int j = i; j < n; j++)
            {
                newArr[j + 1] = arr[j];
            }

            arr = newArr;
        }

        static void DeleteElement(ref int[] arr, int element)
        {
            int n = arr.Length;
            int[] newArr = new int[n - 1];

            // Find the index of the element to be deleted
            int i = 0;
            while (i < n && arr[i] != element)
            {
                newArr[i] = arr[i];
                i++;
            }

            // Copy the remaining elements
            for (int j = i + 1; j < n; j++)
            {
                newArr[j - 1] = arr[j];
            }

            arr = newArr;
        }

        static void UpdateElement(ref int[] arr, int oldElement, int newElement)
        {
            int n = arr.Length;
            int i = 0;
            while (i < n && arr[i] != oldElement)
            {
                i++;
            }

            if (i < n)
            {
                arr[i] = newElement;
            }
        }

        static void ReverseArray(ref int[] arr)
        {
            int n = arr.Length;
            for (int i = 0; i < n / 2; i++)
            {
                int temp = arr[i];
                arr[i] = arr[n - 1 - i];
                arr[n - 1 - i] = temp;
            }
        }

        static int LinearSearch(int[] arr, int element)
        {
            int n = arr.Length;
            for (int i = 0; i < n; i++)
            {
                if (arr[i] == element)
                {
                    return i;
                }
            }
            return -1;
        }

        static int BinarySearch(int[] arr, int element)
        {
            int left = 0;
            int right = arr.Length - 1;

            while (left <= right)
            {
                int mid = left + (right - left) / 2;
                if (arr[mid] == element)
                {
                    return mid;
                }
                else if (arr[mid] < element)
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid - 1;
                }
            }

            return -1;
        }

        static void SortArray(ref int[] arr)
        {
            int n = arr.Length;
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        // Swap arr[j] and arr[j+1]
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
        }
    }
    
}
