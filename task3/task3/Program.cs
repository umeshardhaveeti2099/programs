﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a number: ");
            int number = int.Parse(Console.ReadLine());

            if (number > 0)
            {
                Console.WriteLine($"{number} is a positive number.");
            }
            else if (number < 0)
            {
                Console.WriteLine($"{number} is a negative number.");
            }
            else
            {
                Console.WriteLine($"{number} is zero.");
            }
            Console.WriteLine("\nIterating from the number to zero:");
            if (number >= 0)
            {
                for (int i = number; i >= 0; i--)
                {
                    Console.WriteLine(i);
                     
                }
                Console.ReadLine();
            }
            else
            {
                for (int i = number; i <= 0; i++)
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}

